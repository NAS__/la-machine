import discord
from discord.ext import commands

class Chat(commands.Cog):

    def __init__(self, client):
        self.client = client


    @commands.command()
    async def echo(self, ctx, *, message: str):
        #await ctx.channel.purge(limit = 1)
        await ctx.send(message)

    @commands.command()
    async def spam(self, ctx, n: int, *, message: str):
        i = 0
        if n <= 10:
            for i in range(0, n):
                await ctx.send(message)
                i+=1
        else:
            await ctx.send("Abuse pas frère, 10 maximum.")

    @commands.command()
    async def embed(self, ctx):
        embed = discord.Embed(
            title = "titre",
            description = "description de l'embed",
            color = discord.Color.green()
            )
        embed.set_author(name = "c moi l'auteur")
        embed.set_footer(text = "gros panard")

        await ctx.send(embed = embed)

    @commands.command()
    async def clear(self, ctx, n=5):
        await ctx.channel.purge(limit = n)

def setup(client):
    client.add_cog(Chat(client))