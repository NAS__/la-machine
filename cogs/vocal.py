import discord
from discord.ext import commands

class Vocal(commands.Cog):

    def __init__(self, client):
        self.client = client


    @commands.command()
    async def vocal(self, ctx):
        if ctx.message.author.voice.channel:
            channel = ctx.message.author.voice.channel
            await channel.connect()

    @commands.command()
    async def quitterV(self, ctx):
        server = ctx.message.guild.voice_client
        await server.disconnect()

    @commands.command()
    async def decorecoV(self, ctx):
        server = ctx.message.guild.voice_client
        await server.disconnect()
        channel = ctx.message.author.voice.channel
        await channel.connect()

    @commands.command()
    async def nukeV(self, ctx, n: int):
        i=0
        for i in range(0, n):
            server = ctx.message.guild.voice_client
            await server.disconnect()
            channel = ctx.message.author.voice.channel
            await channel.connect()
            i+=1


def setup(client):
    client.add_cog(Vocal(client))