import discord
import asyncio
import random
from array import *
from discord.ext import commands

class Dehka(commands.Cog):

    def __init__(self, client):
        self.client = client


    @commands.command()
    async def onetwothree(self, ctx):
        await ctx.send(":flag_dz: Viva l'Algérie ! :flag_dz:")

    @commands.command()
    async def blague(self, ctx):
        blagues = [["Qu'est-ce qui est jaune et qui attend ?", "La mère à Bilel"],
        ["Qu'est-ce qu'un chien qui fait des études de médecine ?", "Un dogteur"], 
        ["Sah frero j'ai pas d'inspi là", ""],
        ["Pourquoi le plateau des Minguettes s'appelle comme ça ?", "Parce que quand t'y vas t'es servi. (source: Karim Le Fondateur)"]]
       
        n = random.randint(0, len(blagues)-1)
        await ctx.send(blagues[n][0])
        await asyncio.sleep(5)
        await ctx.send(blagues[n][1])

    @commands.command()
    async def insulte(self, ctx, message:str=""):
        #insultes tirées de https://fr.wiktionary.org/w/index.php?title=Catégorie:Insultes_en_français, jsuis pas responsable de ces propos.
        insultes = ["abruti", "andouille", "assimilé", "avorton", "baleine", "bâtard", "boucaque", "boudin", "bouffon", "BOUFFON", "bougnoule", "branleur", "branlo", "brise-burnes",
        "bulot", "casse-couilles", "cave", "charogne", "chienne", "chiennasse", "cochon", "cochonne", "con", "connard", "connasse", "counifle", "crevure", "enculé", "enfant de putain", 
        "enfant de salope", "enfant de pute", "étron", "face de pet", "fils de bâtard", "fils de pute", "fils de chien", "fils de chienne", "fils de ta race", "fiotte", "fripouille", 
        "fumelard", "fumier", "garage à bite", "garce", "gogole", "gouine", "gourdasse", "gourgandine", "dinde", "dindasse", "gwer", "goï", "grognasse", "halouf", "imbécile", 
        "enculé de ta race", "baiseur de chèvres", "consanguin", "valery", "incapable", "islamo-gauchiste", "jean-foutre", "journalope", "juivaillon", "kahlouche", "lâche", "lopette", 
        "magot", "mange-merde", "marsouin", "mauviette", "merdaille", "merdaillon", "merde", "merdeux", "michtonneuse", "minable", "mollusque", "teckel", "flaque", "mongole", "moule à merde",
        "naze", "nazi", "négraille", "nègre", "négrillon", "négro", "niafou", "nique ta mère", "pédé", "PD", "pédale", "pelle à merde", "petite merde", "pouffiasse", "porc", "poulet", 
        "pute", "putain", "pute borgne", "raclure", "raclure de bidet", "rat", "résidu de capote", "sac à foutre", "sac à merde", "salaud", "salopard", "salope", "SALOOOPE", "sauvage", 
        "ta gueule", "ta mère", "ta race", "tapette", "tarlouze", "tantouze", "tafiole", "teubé", "tête de gland", "thon", "truie", "tocard", "trou du cul", "vide-couilles", "youpin", "zgeg",
        "ravagé"]
        n = random.randint(0, len(insultes)-1)
        await ctx.send(message + " " + insultes[n])


def setup(client):
    client.add_cog(Dehka(client))