import discord
from discord.ext import commands

class Information(commands.Cog):

    def __init__(self, client):
        self.client = client


    @commands.command()
    async def ping(self, ctx):
        embed = discord.Embed(
            title = ":ping_pong: Pong!",
            description = "Latence: {0}".format(round(1000*self.client.latency)) + "ms",
            color = discord.Color.red()
            )

        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(Information(client))