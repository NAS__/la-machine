import discord
from discord.ext import commands

class Status(commands.Cog):

    def __init__(self, client):
        self.client = client


    @commands.Cog.listener()
    async def on_ready(self):
        await self.client.change_presence(status=discord.Status.online, activity=discord.Game("Wsh la zone"))

    @commands.command()
    async def status(self, ctx, *, s: str):
        await self.client.change_presence(activity=discord.Game(s))


def setup(client):
    client.add_cog(Status(client))