import discord
import os
import asyncio
from discord.ext import commands

token = "NzAzMjk5MjQyODI3MDU1MTg0.XqMknA.GGdDMBCvc10L3gxsyTR89z-trpU"

client = commands.Bot(command_prefix = '&')

@client.event
async def on_ready():
    print("Connecté en tant que {0.user}".format(client))

@client.command()
async def load(ctx, extension):
    try:
        client.load_extension(f"cogs.{extension}")
        await ctx.send("Module " + str(extension) + " en ligne :white_check_mark:")
    except:
        await ctx.send("Erreur :exclamation:\nModule " + str(extension) + " introuvable.")


@client.command()
async def unload(ctx, extension):
    try:
        client.unload_extension(f"cogs.{extension}")
        await ctx.send("Module " + extension + " déconnecté :octagonal_sign:")
    except:
        await ctx.send("Erreur :exclamation:\nModule " + str(extension) + " introuvable.")

@client.command()
async def reload (ctx, extension):
    try:
        client.unload_extension(f"cogs.{extension}")
        await ctx.send("Redémarrage module " + extension + " en cours :arrows_counterclockwise:")
        await load(ctx, extension)
    except:
        await ctx.send("Erreur :exclamation:\nModule " + str(extension) + " introuvable.")


for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        client.load_extension(f"cogs.{filename[:-3]}")

client.run(token)